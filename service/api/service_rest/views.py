from django.shortcuts import render
from .models import Technician, Appointment, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from django.shortcuts import get_object_or_404

# Create your views here.
class TechinicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentsListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician"
    ]

    encoders = {
        "technician": TechinicianDetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
           {"technicians":technicians},
            encoder=TechinicianDetailEncoder,

        )
    else:
        content = json.loads(request.body)
        techncian = Technician.objects.create(**content)
        return JsonResponse(
            {"technician": techncian},
            encoder=TechinicianDetailEncoder

        )

@require_http_methods(["DELETE"])
def delete_technician(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Technician.objects.get(employee_id=id).delete()
            return JsonResponse(
                {"Deteled": count > 0},

            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid ID"},
                status = 400
            )


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentsListEncoder,

        )
    else:
        content = json.loads(request.body)
        try:
            id = content['technician']
            content['technician'] = Technician.objects.get(employee_id=id)
            # content['techinican'] = get_object_or_404(Technician,id=id)
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                    appointment,
                    encoder=AppointmentsListEncoder,
                    safe=False,

                )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid ID"},
                status = 400
            )



@require_http_methods(["DELETE"])
def delete_appointment(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Appointment.objects.get(id=id).delete()
            return JsonResponse(
                {"Deleted": count > 0},
                status=200
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid ID"},
                status = 400,
            )


@require_http_methods(["PUT"])
def status_to_cancel(request, id):
    if request.method == "PUT":
        content = {"status": 'canceled'}
        try:
            Appointment.objects.filter(id=id).update(**content)
            appointment = Appointment.objects.get(id=id)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid ID"},
                status = 400,
            )

        return JsonResponse(
            appointment,
            encoder=AppointmentsListEncoder,
            safe=False

        )

@require_http_methods(["PUT"])
def status_to_finish(request, id):
    if request.method == "PUT":
        content = {"status": "finished"}
        try:
            Appointment.objects.filter(id=id).update(**content)
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
            appointment,
            encoder=AppointmentsListEncoder,
            safe=False

        )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid ID"},
                status = 400,
            )
