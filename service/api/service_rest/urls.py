from django.urls import path
from .views import delete_appointment, list_appointments, list_technicians, delete_technician, status_to_cancel, status_to_finish
urlpatterns = [
    path("technicians/", list_technicians, name="list_technician"),
    path("technicians/<int:id>", delete_technician, name="delete_technician"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:id>", delete_appointment, name="delete_appointment"),
    path("appointments/<int:id>/cancel", status_to_cancel, name="status_to_cancel"),
    path("appointments/<int:id>/finish", status_to_finish, name="status_to_finish")
]
