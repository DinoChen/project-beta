import React, { useState, useEffect } from "react";

function formatDate(dateString) {
  // Create a Date object from the input string
  const date = new Date(dateString);

  // Extract year, month, and day components
  const year = date.getFullYear();
  const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Add 1 to month since months are 0-indexed
  const day = date.getDate().toString().padStart(2, "0");

  // Combine the components into the desired format
  return `${month}-${day}-${year}`;
}

// const inputt = "2023-04-25T13:18:00+00:00";
// const formattedDate = formatDate(inputt);

function formatTime(dateString) {
  // Create a Date object from the input string
  const date = new Date(dateString);

  // Extract hour, minute, and second components
  let hours = date.getUTCHours();
  console.log(hours);
  const minutes = date.getMinutes().toString().padStart(2, "0");
  const seconds = date.getSeconds().toString().padStart(2, "0");
  console.log("minutes", minutes);
  console.log("seconds", seconds);
  // Determine AM or PM
  const amOrPm = hours >= 12 ? "PM" : "AM";

  // Convert to 12-hour format
  hours = hours % 12;
  hours = hours ? hours : 12; // Replace 0 with 12 for display
  const hoursString = hours.toString().padStart(2, "0");

  // Combine the components into the desired format
  return `${hoursString}:${minutes}:${seconds} ${amOrPm}`;
}

const input = "2023-04-25T13:18:00+00:00";
const formattedTime = formatTime(input);
console.log(formattedTime); // Output: "01:18:00 PM"
const fetchConfig = {
  method: "GET",
  headers: {
    "Content-Type": "application/json",
  },
};

function AppointmentList(props) {
  const [appointments, setAppointments] = useState(props.list);
  const [vipVins, setVipVins] = useState([]);

  useEffect(() => {
    fetchVIPs();
  }, []);

  async function fetchVIPs() {
    try {
      const response = await fetch(
        `http://localhost:8100/api/automobiles/`,
        fetchConfig
      );

      if (response.ok) {
        const content = await response.json();
        const vips = content.autos.map((auto) => auto.vin);
        setVipVins(vips);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error("Error fetching VIPs:", error);
    }
  }

  function vipVin(vin) {
    return vipVins.includes(vin) ? "Yes" : "No";
  }
  async function cancelAppointment(id) {
    const fetchConfig = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    };
    await fetch(
      `http://localhost:8080/api/appointments/${id}/cancel`,
      fetchConfig
    );
    setAppointments(appointments.filter((a) => a.id !== id));
  }

  function finishAppointment(id) {
    const fetchConfig = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    };
    fetch(`http://localhost:8080/api/appointments/${id}/finish`, fetchConfig);
    setAppointments(appointments.filter((a) => a.id !== id));
  }

  return (
    <>
      <div className="mt-5">
        <h1>Service appointments</h1>
        <table className="table table-hover  table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Is VIP</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technican</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {appointments.map((appointment) => {
              console.log(appointment);
              if (String(appointment.status) === "unfinished") {
                return (
                  <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{vipVin(appointment.vin)}</td>
                    <td>{appointment.customer}</td>
                    <td>{formatDate(appointment.date_time)}</td>
                    <td>{formatTime(appointment.date_time)}</td>
                    <td>
                      {appointment.technician.first_name}{" "}
                      {appointment.technician.last_name}{" "}
                    </td>
                    <td>{appointment.reason}</td>
                    <button
                      type="button"
                      className="btn btn-danger"
                      onClick={() => cancelAppointment(appointment.id)}
                    >
                      Cancel
                    </button>
                    <button
                      type="button"
                      className="btn btn-success"
                      onClick={() => finishAppointment(appointment.id)}
                    >
                      Finish
                    </button>
                  </tr>
                );
              }
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default AppointmentList;
