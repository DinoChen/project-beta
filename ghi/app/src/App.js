import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianForm from "./TechnicianForm";
import TechnicansList from "./TechniciansList";
import AppointmentForm from "./AppointmentForm";
import AppointmentList from "./AppointmentsList";
import ServiceHistory from "./ServiceHistory";
import ManufacturersList from "./ManufacturersList";
import ManufacturerForm from "./ManufacturerForm";
import ModelForm from "./ModelForm";
import ModelsList from "./ModelsList";
import AutomobilesList from "./AutomobilesList";
import AutomobileForm from "./AutomobileForm";
import SalespersonForm from "./SalespersonForm";
import SalespeopleList from "./SalespeopleList";
import CustomerForm from "./CustomerForm";
import CustomersList from "./CustomersList";
import SaleForm from "./SaleForm";
import SalesList from "./SalesList";
import SalespersonHistory from "./SalespersonHistory";
import Footer from "./Footer";
import "./App.css";
function App(props) {
  if (
    props.technicians === undefined ||
    props.appointments === undefined ||
    props.automobiles === undefined ||
    props.salespeople === undefined ||
    props.sales === undefined
  ) {
    console.log("test app not work");
    console.log(props.salespeople);
    return null;
  }
  console.log("test app work");

  return (
    <BrowserRouter>
      <Nav />
      <div className="container app-container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="manufacturers">
            <Route path="new" element={<ManufacturerForm />}></Route>
            <Route
              path="list"
              element={<ManufacturersList list={props.manufacturers} />}
            ></Route>
          </Route>
          <Route path="models">
            <Route path="new" element={<ModelForm />}></Route>
            <Route
              path="list"
              element={<ModelsList list={props.models} />}
            ></Route>
          </Route>

          <Route path="automobiles">
            <Route path="new" element={<AutomobileForm />}></Route>
            <Route
              path="list"
              element={<AutomobilesList list={props.automobiles} />}
            ></Route>
          </Route>

          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />}></Route>
            <Route
              path="list"
              element={<TechnicansList list={props.technicians} />}
            ></Route>
          </Route>
          <Route path="appointments">
            <Route path="new" element={<AppointmentForm />}></Route>
            <Route
              path="service"
              element={<AppointmentList list={props.appointments} />}
            ></Route>
            <Route
              path="service/history"
              element={<ServiceHistory list={props.appointments} />}
            ></Route>
          </Route>
          <Route path="salespeople">
            <Route path="new" element={<SalespersonForm />}></Route>
            <Route
              path="list"
              element={<SalespeopleList list={props.salespeople} />}
            ></Route>
          </Route>

          <Route path="customers">
            <Route path="new" element={<CustomerForm />}></Route>
            <Route
              path="list"
              element={<CustomersList list={props.customers} />}
            ></Route>
          </Route>

          <Route path="sales">
            <Route path="new" element={<SaleForm />}></Route>
            <Route
              path="list"
              element={<SalesList list={props.sales} />}
            ></Route>
            <Route
              path="history"
              element={<SalespersonHistory list={props.sales} />}
            ></Route>
          </Route>
        </Routes>
      </div>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
