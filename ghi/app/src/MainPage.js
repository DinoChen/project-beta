function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership management!
        </p>
        <img
          src="https://i.imgur.com/HclMof5.gif"
          style={{ maxWidth: "500px", maxHeight: "500px" }}
        ></img>
      </div>
    </div>
  );
}

export default MainPage;
