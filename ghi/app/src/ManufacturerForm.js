import React from "react";

class ManufacturerForm extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    const url = "http://localhost:8100/api/manufacturers/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const cleared = {
        name: "",
      };
      this.setState(cleared);
    }
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a manufacturer</h1>

              <form onSubmit={this.handleSubmit} id="create-hat-form">
                <input
                  onChange={this.handleNameChange}
                  value={this.state.name}
                  placeholder="Manufacturer name..."
                  required
                  type="text"
                  className="form-control form-floating mb-3"
                />
                {/* <div className="form-floating mb-3">
                  <input
                    onChange={this.handleNameChange}
                    value={this.state.name}
                    placeholder="name"
                    required
                    type="text"
                    className="form-control"
                  />

                </div> */}

                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ManufacturerForm;
