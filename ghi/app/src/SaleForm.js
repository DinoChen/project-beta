import React from "react";

class SaleForm extends React.Component {
  constructor() {
    super();
    this.state = {
      automobile: "",
      automobiles: [],
      price: "",
      salesperson: "",
      salespeople: [],
      customer: "",
      customers: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
    this.handleSalespersonChange = this.handleSalespersonChange.bind(this);
    this.handlePriceChange = this.handlePriceChange.bind(this);
    this.handleCustomerChange = this.handleCustomerChange.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    // const autoData = {};

    console.log("does prent submit work?");
    const data = { ...this.state };
    delete data.customers;
    delete data.automobiles;
    delete data.salespeople;
    console.log(data);
    const url = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const automobile_url = `http://localhost:8100/api/automobiles/${this.state.automobile}/`;
    const autoData = {};
    autoData.sold = true;
    const fetchConfig2 = {
      method: "PUT",
      body: JSON.stringify(autoData),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const automobileResponse = await fetch(automobile_url, fetchConfig2);
    console.log("after fetch", automobileResponse.json());
    if (automobileResponse.ok) {
      console.log("it's been sold");
    } else {
      console.log("not been sold");
    }

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      console.log("response worked");
    } else {
      console.log("response not work");
    }

    const cleared = {
      automobile: "",
      automobiles: [],
      salesperson: "",
      salespeople: [],
      price: "",
      customers: [],
      customer: "",
    };
    // window.location.reload();
    this.setState(cleared);
  }

  handleSalespersonChange(event) {
    const value = event.target.value;
    this.setState({ salesperson: value });
  }
  handlePriceChange(event) {
    const value = event.target.value;
    this.setState({ price: value });
  }
  handleAutomobileChange(event) {
    const value = event.target.value;
    this.setState({ automobile: value });
  }

  handleCustomerChange(event) {
    const value = event.target.value;
    this.setState({ customer: value });
  }

  async componentDidMount() {
    const automobile_url = "http://localhost:8100/api/automobiles/";
    const salespeople_url = "http://localhost:8090/api/salespeople/";
    const customer_url = "http://localhost:8090/api/customers/";

    const automobileResponse = await fetch(automobile_url);
    const salespeopleResponse = await fetch(salespeople_url);
    const customerResponse = await fetch(customer_url);
    if (
      automobileResponse.ok &&
      salespeopleResponse.ok &&
      customerResponse.ok
    ) {
      const automobileData = await automobileResponse.json();
      const salespeopleData = await salespeopleResponse.json();
      const customersData = await customerResponse.json();

      console.log(automobileData);
      console.log("response work");
      this.setState({
        customers: customersData.customers,
        automobiles: automobileData.autos,
        salespeople: salespeopleData.salespeople,
      });
    } else {
      console.log("response not work");
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Record a new sale</h1>
              <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="mb-3">
                  <label htmlFor="automobile">Automobile VIN</label>
                  <select
                    onChange={this.handleAutomobileChange}
                    value={this.state.automobile}
                    required
                    id="vin"
                    name="vin"
                    className="form-select"
                  >
                    <option value="">Choose an automobile VIN...</option>
                    {this.state.automobiles.map((automobile) => {
                      if (automobile.sold === false) {
                        return (
                          <option key={automobile.vin}>{automobile.vin}</option>
                        );
                      }
                    })}
                  </select>
                </div>

                <div className="mb-3">
                  <label>Salesperson</label>
                  <select
                    onChange={this.handleSalespersonChange}
                    value={this.state.salesperson}
                    required
                    className="form-select"
                  >
                    <option value="">Choose a salesperson...</option>
                    {this.state.salespeople.map((salesperson) => {
                      return (
                        <option
                          key={salesperson.employee_id}
                          value={salesperson.employee_id}
                        >
                          {salesperson.first_name} {""} {salesperson.last_name}
                        </option>
                      );
                    })}
                  </select>
                </div>

                <div className="mb-3">
                  <label>Customer</label>
                  <select
                    onChange={this.handleCustomerChange}
                    value={this.state.customer}
                    required
                    className="form-select"
                  >
                    <option value="">Choose a customer...</option>
                    {this.state.customers.map((customer) => {
                      return (
                        <option key={customer.id} value={customer.id}>
                          {customer.first_name} {""} {customer.last_name}
                        </option>
                      );
                    })}
                  </select>
                </div>

                <div className=" mb-3">
                  <label htmlFor="price">Price</label>
                  <input
                    onChange={this.handlePriceChange}
                    value={this.state.price}
                    placeholder="0"
                    required
                    type="number"
                    name="price"
                    id="price"
                    className="form-control"
                  />
                </div>

                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SaleForm;

// import React, { useState, useEffect } from "react";

// function SaleForm() {
//   const [automobiles, setAutomobiles] = useState([]);

//   const [salespeople, setSalespeople] = useState([]);

//   const [customers, setCustomers] = useState([]);

//   const [automobile, setAutomobile] = useState("");
//   const handleAutomobileChange = (event) => {
//     const value = event.target.value;
//     setAutomobile(value);
//   };

//   const [salesperson, setSalesperson] = useState("");
//   const handleSalespersonChange = (event) => {
//     const value = event.target.value;
//     setSalesperson(value);
//   };

//   const [customer, setCustomer] = useState("");
//   const handleCustomerChange = (event) => {
//     const value = event.target.value;
//     setCustomer(value);
//   };

//   const [price, setPrice] = useState("");
//   const handlePriceChange = (event) => {
//     const value = event.target.value;
//     setPrice(value);
//   };

//   const handleSubmit = async (event) => {
//     event.preventDefault();

//     const autoData = {};
//     const saleData = {};

//     saleData.automobile = automobile;
//     saleData.salesperson = salesperson;
//     saleData.customer = customer;
//     saleData.price = price;

//     autoData.sold = true;

//     const saleUrl = "http://localhost:8090/api/sales/";
//     const saleFetchConfig = {
//       method: "post",
//       body: JSON.stringify(saleData),
//       headers: {
//         "Content-Type": "application/json",
//       },
//     };
//     const autoUrl = `http://localhost:8100/api/automobiles/${automobile}/`;
//     const autoFetchConfig = {
//       method: "put",
//       body: JSON.stringify(autoData),
//       headers: {
//         "Content-Type": "application/json",
//       },
//     };
//     const saleResponse = await fetch(saleUrl, saleFetchConfig);
//     const autoResponse = await fetch(autoUrl, autoFetchConfig);
//     if (saleResponse.ok && autoResponse.ok) {
//       setAutomobile("");
//       setSalesperson("");
//       setCustomer("");
//       setPrice("");
//     }
//   };

//   const fetchData = async () => {
//     const automobileUrl = "http://localhost:8100/api/automobiles/";
//     const salespeopleUrl = "http://localhost:8090/api/salespeople/";
//     const customersUrl = "http://localhost:8090/api/customers/";

//     const automobileResponse = await fetch(automobileUrl);
//     const salespeopleResponse = await fetch(salespeopleUrl);
//     const customerResponse = await fetch(customersUrl);

//     if (
//       automobileResponse.ok &&
//       salespeopleResponse.ok &&
//       customerResponse.ok
//     ) {
//       const automobileData = await automobileResponse.json();
//       const salespeopleData = await salespeopleResponse.json();
//       const customersData = await customerResponse.json();
//       setAutomobiles(automobileData.autos);
//       setSalespeople(salespeopleData.salespeople);
//       setCustomers(customersData.customers);
//     }
//   };
//   useEffect(() => {
//     fetchData();
//   }, []);

//   return (
//     <div className="row">
//       <div className="offset-3 col-6">
//         <div className="shadow p-4 mt-4">
//           <h1>Record a new sale</h1>
//           <form onSubmit={handleSubmit} id="create-sale-form">
//             <div className="mb-3">
//               <label htmlFor="automobile">Automobile VIN</label>
//               <select
//                 value={automobile}
//                 onChange={handleAutomobileChange}
//                 required
//                 id="vin"
//                 name="vin"
//                 className="form-select"
//               >
//                 <option value="">Choose an automobile VIN</option>
//                 {automobiles.map((auto) => {
//                   if (auto.sold === false) {
//                     return (
//                       <option key={auto.vin} value={auto.vin}>
//                         {auto.vin}
//                       </option>
//                     );
//                   }
//                 })}
//               </select>
//             </div>
//             <div className="mb-3">
//               <label htmlFor="salesperson">Salesperson</label>
//               <select
//                 value={salesperson}
//                 onChange={handleSalespersonChange}
//                 required
//                 id="salesperson"
//                 name="salesperson"
//                 className="form-select"
//               >
//                 <option value="">Choose a salesperson</option>
//                 {salespeople.map((salesperson) => {
//                   return (
//                     <option
//                       key={salesperson.employee_id}
//                       value={salesperson.employee_id}
//                     >
//                       {salesperson.first_name + " " + salesperson.last_name}
//                     </option>
//                   );
//                 })}
//               </select>
//             </div>
//             <div className="mb-3">
//               <label htmlFor="customer">Customer</label>
//               <select
//                 value={customer}
//                 onChange={handleCustomerChange}
//                 required
//                 id="customer"
//                 name="customer"
//                 className="form-select"
//               >
//                 <option value="">Choose a customer</option>
//                 {customers.map((customer) => {
//                   return (
//                     <option key={customer.id} value={customer.id}>
//                       {customer.first_name + " " + customer.last_name}
//                     </option>
//                   );
//                 })}
//               </select>
//             </div>
//             <div className="mb-3">
//               <label htmlFor="price">Price</label>
//               <input
//                 value={price}
//                 onChange={handlePriceChange}
//                 placeholder="0"
//                 required
//                 type="number"
//                 name="price"
//                 id="price"
//                 className="form-control"
//               />
//             </div>
//             <button className="btn btn-primary">Create</button>
//           </form>
//         </div>
//       </div>
//     </div>
//   );
// }
// export default SaleForm;
