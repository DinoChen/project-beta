import React from "react";

export default function SalesList(props) {
  return (
    <>
      <div className="mt-5">
        <h1>Sales List</h1>
        <table className="table table-hover  table-striped">
          <thead>
            <tr>
              <th>Salesperson Employee ID</th>
              <th>Salesperson Name</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {props.list.map((sale) => {
              return (
                <tr key={sale.id}>
                  <td>{sale.salesperson.employee_id}</td>
                  <td>
                    {sale.salesperson.first_name} {sale.salesperson.last_name}
                  </td>
                  <td>
                    {sale.customer.first_name}
                    {sale.customer.last_name}
                  </td>
                  <td>{sale.automobile.vin}</td>
                  <td>${sale.price}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
