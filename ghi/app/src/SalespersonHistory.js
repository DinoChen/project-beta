import React from "react";
import { useState, useEffect } from "react";

export default function SalespersonHistory(props) {
  const [searchInput, setSearchInput] = useState("");

  function handleSearchInput(e) {
    setSearchInput(e.target.value);
  }

  const filteredList = props.list.filter((sale) =>
    sale.automobile.vin.toLowerCase().includes(searchInput.toLowerCase())
  );

  return (
    <>
      <div className="mt-5">
        <h1>Service History</h1>
        <input
          type="text"
          className="form-control mt-3 mb-3"
          placeholder="Search by VIN"
          value={searchInput}
          onChange={handleSearchInput}
        />
        <table className="table table-hover table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>

              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {filteredList.map((sale) => {
              return (
                <tr key={sale.id}>
                  <td>
                    {sale.salesperson.first_name}
                    {sale.salesperson.last_name}
                  </td>
                  <td>
                    {sale.customer.first_name} {sale.customer.last_name}{" "}
                  </td>
                  <td>{sale.automobile.vin}</td>
                  <td>{sale.price}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
