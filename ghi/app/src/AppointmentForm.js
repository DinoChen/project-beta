import React from "react";

class AppointmentForm extends React.Component {
  constructor() {
    super();
    this.state = {
      vin: "",
      customer: "",
      date_time: "",
      technician: "",
      technicians: [],
      reason: "",
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleVinChange = this.handleVinChange.bind(this);
    this.handleCustomerChange = this.handleCustomerChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
    this.handleReasonChange = this.handleReasonChange.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.technicians;
    console.log(data);
    const url = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    console.log("stringfy", data);

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const cleared = {
        vin: "",
        customer: "",
        date_time: "",
        technician: "",
        technicians: [],
        reason: "",
      };
      window.location.reload();
      this.setState(cleared);
    } else {
      console.log("response not work");
    }
  }

  handleCustomerChange(event) {
    const value = event.target.value;
    this.setState({ customer: value });
  }
  handleDateChange(event) {
    const value = event.target.value;
    this.setState({ date_time: value });
  }
  handleVinChange(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  handleTechnicianChange(event) {
    const value = event.target.value;
    this.setState({ technician: value });
  }

  handleReasonChange(event) {
    const value = event.target.value;
    this.setState({ reason: value });
  }

  async componentDidMount() {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new appointment</h1>
              <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleVinChange}
                    value={this.state.vin}
                    placeholder="Automobile VIN"
                    required
                    type="text"
                    name="Automobile VIN"
                    id="Automobile VIN"
                    className="form-control"
                  />
                  <label htmlFor="Automobile VIN">Automobile VIN</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleCustomerChange}
                    value={this.state.customer}
                    placeholder="Customer"
                    required
                    type="text"
                    name="Customer"
                    id="Customer"
                    className="form-control"
                  />
                  <label htmlFor="Customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleDateChange}
                    value={this.state.date_time}
                    placeholder="Date and Time"
                    required
                    type="datetime-local"
                    name="Date and Time"
                    id="Date and Time"
                    className="form-control"
                  />
                  <label htmlFor="Date and Time">Date and Time</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleReasonChange}
                    value={this.state.reason}
                    placeholder="Reason"
                    required
                    type="test"
                    name="Reason"
                    id="Reason"
                    className="form-control"
                  />
                  <label htmlFor="Reason">Reason</label>
                </div>

                <div className="mb-3">
                  <select
                    onChange={this.handleTechnicianChange}
                    value={this.state.technician}
                    required
                    id="technician"
                    name="technician"
                    className="form-select"
                  >
                    <option value="">Choose a technician</option>
                    {this.state.technicians.map((technician) => {
                      return (
                        <option key={technician.id} value={technician.id}>
                          ID:{technician.id} {technician.first_name}{" "}
                          {technician.last_name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AppointmentForm;
