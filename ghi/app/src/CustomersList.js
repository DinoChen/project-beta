import React from "react";

export default function CustomersList(props) {
  return (
    <>
      <div className="mt-5">
        <h1>Customers List</h1>
        <table className="table table-hover  table-striped">
          <thead>
            <tr>
              <th>Fist Name</th>
              <th>Last Name</th>
              <th>Phone Number</th>
              <th>Address</th>
            </tr>
          </thead>
          <tbody>
            {props.list.map((customer) => {
              return (
                <tr key={customer.id}>
                  <td>{customer.first_name}</td>
                  <td>{customer.last_name}</td>
                  <td>{customer.phone_number}</td>
                  <td>{customer.address}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
