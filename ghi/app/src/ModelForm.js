import React from "react";
class ModelForm extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      picture_url: "",
      manufacturer_id: "",
      manufacturers: [],
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleUrlChange = this.handleUrlChange.bind(this);
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    console.log("model submit form");
    const data = { ...this.state };

    delete data.manufacturers;
    const url = "http://localhost:8100/api/models/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const cleared = {
        name: "",
        picture_url: "",
        manufacturer_id: "",
        manufacturers: [],
      };
      this.setState(cleared);
    }
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }
  handleUrlChange(event) {
    const value = event.target.value;
    this.setState({ picture_url: value });
  }
  handleManufacturerChange(event) {
    const value = event.target.value;
    console.log("m change");
    this.setState({ manufacturer_id: value });
  }

  async componentDidMount() {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers });
      console.log("test response ok");
      console.log(data.manufacturers);
    } else {
      console.log("not ok");
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a vehicle model</h1>

              <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="mb-3">
                  <input
                    onChange={this.handleNameChange}
                    value={this.state.name}
                    placeholder="Model name..."
                    required
                    type="text"
                    className="form-control form-floating mb-3"
                  />
                </div>
                <div className="mb-3">
                  <input
                    onChange={this.handleUrlChange}
                    value={this.state.picture_url}
                    placeholder="Picture URL..."
                    required
                    type="url"
                    className="form-control form-floating mb-3"
                  />
                </div>
                <div className="mb-3">
                  <select
                    onChange={this.handleManufacturerChange}
                    value={this.state.manufacturer_id.id}
                    required
                    id="manufacturer"
                    name="manufacturer"
                    className="form-select"
                  >
                    <option value="">Choose a manufacturer</option>

                    {this.state.manufacturers.map((m) => {
                      return (
                        <option key={m.id} value={m.id}>
                          {m.name}
                        </option>
                      );
                    })}
                  </select>
                </div>

                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ModelForm;
