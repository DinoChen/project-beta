import React from "react";

class SalespersonForm extends React.Component {
  constructor() {
    super();
    this.state = {
      first_name: "",
      last_name: "",
      employee_id: "",
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleEmployeeIDChange = this.handleEmployeeIDChange.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    const url = "http://localhost:8090/api/salespeople/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const cleared = {
        first_name: "",
        last_name: "",
        employee_id: "",
      };
      this.setState(cleared);
    }
  }

  handleFirstNameChange(event) {
    const value = event.target.value;
    this.setState({ first_name: value });
  }
  handleLastNameChange(event) {
    const value = event.target.value;
    this.setState({ last_name: value });
  }
  handleEmployeeIDChange(event) {
    const value = event.target.value;
    this.setState({ employee_id: value });
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a salespeople</h1>
              <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleFirstNameChange}
                    value={this.state.first_name}
                    placeholder="First name..."
                    required
                    type="text"
                    name="FristName"
                    id="FirstName"
                    className="form-control"
                  />
                  <label htmlFor="FirstName">First name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleLastNameChange}
                    value={this.state.last_name}
                    placeholder="Last name..."
                    required
                    type="text"
                    name="LastName"
                    id="LastName"
                    className="form-control"
                  />
                  <label htmlFor="LastName">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleEmployeeIDChange}
                    value={this.state.employee_id}
                    placeholder="Employee ID..."
                    required
                    type="text"
                    name="EmployeeID"
                    id="EmployeeID"
                    className="form-control"
                  />
                  <label htmlFor="EmployeeID">Employee ID</label>
                </div>

                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SalespersonForm;

// import React from "react";

// class SalespersonForm extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       first_name: "",
//       last_name: "",
//       employee_id: "",
//     };
//     this.handleSubmit = this.handleSubmit.bind(this);
//     this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
//     this.handleLastNameChange = this.handleLastNameChange.bind(this);
//     this.handleEmployeeIDChange = this.handleEmployeeIDChange.bind(this);
//   }

//   async handleSubmit(event) {
//     event.preventDefault();
//     const data = { ...this.state };

//     console.log("test", data);

//     const url = "http://localhost:8080/api/technicians/";
//     const fetchConfig = {
//       method: "POST",
//       body: JSON.stringify(data),
//       headers: {
//         "Content-Type": "application/json",
//       },
//     };

//     const response = await fetch(url, fetchConfig);
//     if (response.ok) {
//       const cleared = {
//         first_name: "",
//         last_name: "",
//         employee_id: "",
//       };

//       this.setState(cleared);
//     }
//   }

//   handleFirstNameChange(event) {
//     const value = event.target.value;
//     this.setState({ first_name: value });
//   }
//   handleLastNameChange(event) {
//     const value = event.target.value;
//     this.setState({ last_name: value });
//   }
//   handleEmployeeIDChange(event) {
//     const value = event.target.value;
//     this.setState({ employee_id: value });
//   }

//   render() {
//     return (
//       <React.Fragment>
//         <div className="row">
//           <div className="offset-3 col-6">
//             <div className="shadow p-4 mt-4">
//               <h1>Add a Technican</h1>
//               <form onSubmit={this.handleSubmit} id="create-hat-form">
//                 <div className="form-floating mb-3">
//                   <input
//                     onChange={this.handleFirstNameChange}
//                     value={this.state.first_name}
//                     placeholder="First name..."
//                     required
//                     type="text"
//                     name="FristName"
//                     id="FirstName"
//                     className="form-control"
//                   />
//                   <label htmlFor="FirstName">First name</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                   <input
//                     onChange={this.handleLastNameChange}
//                     value={this.state.LastName}
//                     placeholder="Last name..."
//                     required
//                     type="text"
//                     name="LastName"
//                     id="LastName"
//                     className="form-control"
//                   />
//                   <label htmlFor="LastName">Last Name</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                   <input
//                     onChange={this.handleEmployeeIDChange}
//                     value={this.state.employee_id}
//                     placeholder="Employee ID..."
//                     required
//                     type="text"
//                     name="EmployeeID"
//                     id="EmployeeID"
//                     className="form-control"
//                   />
//                   <label htmlFor="EmployeeID">Employee ID</label>
//                 </div>

//                 <button className="btn btn-primary">Create</button>
//               </form>
//             </div>
//           </div>
//         </div>
//       </React.Fragment>
//     );
//   }
// }

// export default SalespersonForm;
