import React from "react";

class CustomerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: "",
      last_name: "",
      address: "",
      phone_number: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleAddressChange = this.handleAddressChange.bind(this);
    this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };

    console.log("test", data);

    const url = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const cleared = {
        first_name: "",
        last_name: "",
        address: "",
        phone_number: "",
      };

      this.setState(cleared);
    }
  }

  handleFirstNameChange(event) {
    const value = event.target.value;
    this.setState({ first_name: value });
  }
  handleLastNameChange(event) {
    const value = event.target.value;
    this.setState({ last_name: value });
  }
  handleAddressChange(event) {
    const value = event.target.value;
    this.setState({ address: value });
  }
  handlePhoneNumberChange(event) {
    const value = event.target.value;
    this.setState({ phone_number: value });
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Customer</h1>
              <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleFirstNameChange}
                    value={this.state.first_name}
                    placeholder="First name..."
                    required
                    type="text"
                    name="FristName"
                    id="FirstName"
                    className="form-control"
                  />
                  <label htmlFor="FirstName">First name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleLastNameChange}
                    value={this.state.LastName}
                    placeholder="Last name..."
                    required
                    type="text"
                    name="LastName"
                    id="LastName"
                    className="form-control"
                  />
                  <label htmlFor="LastName">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleAddressChange}
                    value={this.state.address}
                    placeholder="Address..."
                    required
                    type="text"
                    name="Address"
                    id="Address"
                    className="form-control"
                  />
                  <label htmlFor="Address">Address</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handlePhoneNumberChange}
                    value={this.state.phone_number}
                    placeholder="Phone number..."
                    required
                    type="text"
                    className="form-control"
                  />
                  <label>Phone number</label>
                </div>

                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CustomerForm;
