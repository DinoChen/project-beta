import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <div className="page-container white-text">
      <div className="content-wrap">{/* Your main content goes here */}</div>
      <footer className="footer bg-success navbar-dark content-wrap">
        <div className="container grid grid--footer">
          {/* Footer content goes here */}
          <div class="logo-col">
            <div className="footer-link-col">
              <a className="footer-link" href="#">
                <ion-icon
                  className="social-icon"
                  name="logo-instagram"
                ></ion-icon>
              </a>

              <a className="footer-link" href="#">
                <ion-icon
                  className="social-icon"
                  name="logo-facebook"
                ></ion-icon>
              </a>

              <a className="footer-link" href="#">
                <ion-icon
                  className="social-icon"
                  name="logo-twitter"
                ></ion-icon>
              </a>
            </div>
            <p class="copyright">
              Copyright &copy; <span class="year">2027</span> by Omnifood, Inc.
              All rights reserved.
            </p>
          </div>

          <div class="address-col">
            <p className="footer-heading">Contact us</p>

            <p class="address">308 110th Ave NE, Bellevue, WA 94107</p>

            <h6 class="footer-link" href="tel:415-201-6370">
              425-394-6818
            </h6>

            <h6 class="footer-link" href="mailto:hello@omnifood.com">
              hello@Carcar.com
            </h6>
          </div>
          {/*
          <nav class="nav-col">
            <p class="footer-heading">Account</p>
            <ul class="footer-nav">
              <li>
                <h6 class="footer-link" href="#">
                  Create account
                </h6>
              </li>
              <li>
                <h6 class="footer-link" href="#">
                  Sign in
                </h6>
              </li>
              <li>
                <h6 class="footer-link" href="#">
                  iOS app
                </h6>
              </li>
              <li>
                <h6 class="footer-link" href="#">
                  Android app
                </h6>
              </li>
            </ul>
          </nav> */}

          <nav class="nav-col">
            <p class="footer-heading">Account</p>

            <h6 className="footer-link" href="#">
              Create account
            </h6>

            <h6 className="footer-link" href="#">
              Sign in
            </h6>

            <h6 className="footer-link" href="#">
              iOS app
            </h6>

            <h6 class="footer-link" href="#">
              Android app
            </h6>
          </nav>

          {/* <nav class="nav-col">
            <p class="footer-heading">Company</p>
            <ul class="footer-nav">
              <li>
                <h6 class="footer-link" href="#">
                  About Carcar
                </h6>
              </li>
              <li>
                <h6 class="footer-link" href="#">
                  For Business
                </h6>
              </li>
              <li>
                <h6 class="footer-link" href="#">
                  Partners
                </h6>
              </li>
              <li>
                <h6 class="footer-link" href="#">
                  Careers
                </h6>
              </li>
            </ul>
          </nav> */}

          <nav class="nav-col">
            <p class="footer-heading">Company</p>

            <h6 class="footer-link" href="#">
              About Carcar
            </h6>

            <h6 class="footer-link" href="#">
              For Business
            </h6>

            <h6 class="footer-link" href="#">
              Partners
            </h6>

            <h6 class="footer-link" href="#">
              Careers
            </h6>
          </nav>

          <nav class="nav-col">
            <p class="footer-heading">Resources</p>

            <h6 class="footer-link" href="#">
              Parts directory{" "}
            </h6>

            <h6 class="footer-link" href="#">
              Help center
            </h6>

            <h6 class="footer-link" href="#">
              Privacy & terms
            </h6>
          </nav>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
