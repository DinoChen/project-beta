import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadTechnicians() {
  const technicianResponse = await fetch(
    "http://localhost:8080/api/technicians/"
  );
  const appointmentsResponse = await fetch(
    "http://localhost:8080/api/appointments/"
  );

  const manufactuerersResponse = await fetch(
    "http://localhost:8100/api/manufacturers/"
  );

  const modelsResponse = await fetch("http://localhost:8100/api/models/");
  const automobilesResponse = await fetch(
    "http://localhost:8100/api/automobiles/"
  );
  const salespeopleResponse = await fetch(
    "http://localhost:8090/api/salespeople/"
  );
  const customersResponse = await fetch("http://localhost:8090/api/customers/");
  const salesResponse = await fetch("http://localhost:8090/api/sales/");

  if (
    technicianResponse.ok &&
    appointmentsResponse.ok &&
    manufactuerersResponse.ok &&
    modelsResponse.ok &&
    salespeopleResponse.ok &&
    customersResponse.ok &&
    salesResponse.ok
  ) {
    const technicianData = await technicianResponse.json();
    const appointmentData = await appointmentsResponse.json();
    const manufactuerersData = await manufactuerersResponse.json();
    const modelsData = await modelsResponse.json();
    const automobilesData = await automobilesResponse.json();
    const salespeopleData = await salespeopleResponse.json();
    const customersData = await customersResponse.json();
    const salesData = await salesResponse.json();
    console.log("teest");
    console.log(technicianData.technicians);
    console.log(appointmentData.appointments);
    console.log(automobilesData);
    console.log(salespeopleData.salespeople);
    root.render(
      <React.StrictMode>
        <App
          technicians={technicianData.technicians}
          appointments={appointmentData.appointments}
          manufacturers={manufactuerersData.manufacturers}
          models={modelsData.models}
          automobiles={automobilesData.autos}
          salespeople={salespeopleData.salespeople}
          customers={customersData.customers}
          sales={salesData.sales}
        />
      </React.StrictMode>
    );
  } else {
    console.log("not work");
    console.error(technicianResponse);
  }
}

loadTechnicians();
