import React from "react";

class AutomobileForm extends React.Component {
  constructor() {
    super();
    this.state = {
      color: "",
      year: "",
      vin: "",
      model_id: "",
      models: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleVinChange = this.handleVinChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);
    this.handleModelChange = this.handleModelChange.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.models;
    console.log(data);
    const url = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const cleared = {
        vin: "",
        color: "",
        year: "",

        models: [],
        model_id: "",
      };
      window.location.reload();
      this.setState(cleared);
    } else {
      console.log("response not work");
    }
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }
  handleYearChange(event) {
    const value = event.target.value;
    this.setState({ year: value });
  }
  handleVinChange(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  handleModelChange(event) {
    const value = event.target.value;
    this.setState({ model_id: value });
  }

  async componentDidMount() {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add an automobile to inventory</h1>
              <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleVinChange}
                    value={this.state.vin}
                    placeholder="Automobile VIN"
                    required
                    type="text"
                    name="Automobile VIN"
                    id="Automobile VIN"
                    className="form-control"
                  />
                  <label htmlFor="Automobile VIN">Automobile VIN</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleColorChange}
                    value={this.state.color}
                    placeholder="Color"
                    required
                    type="text"
                    name="color"
                    id="color"
                    className="form-control"
                  />
                  <label htmlFor="Customer">color</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleYearChange}
                    value={this.state.year}
                    placeholder="Year..."
                    required
                    type="number"
                    name="year"
                    className="form-control"
                  />
                  <label htmlFor="year">year</label>
                </div>

                <div className="mb-3">
                  <select
                    onChange={this.handleModelChange}
                    value={this.state.model_id}
                    required
                    id="model"
                    name="model"
                    className="form-select"
                  >
                    <option value="">Choose a model</option>
                    {this.state.models.map((model) => {
                      return (
                        <option key={model.id} value={model.id}>
                          {model.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AutomobileForm;
