function AutomobilesList(props) {
  function is_sold(sold) {
    return sold ? "Yes" : "No";
  }
  return (
    <>
      <div className="mt-5">
        <h1>Automobiles</h1>
        <table className="table table-hover  table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Color</th>
              <th>Year</th>
              <th>Model</th>
              <th>Manufacturer</th>
              <th>Sold</th>
            </tr>
          </thead>
          <tbody>
            {props.list.map((automobile) => {
              console.log(automobile);
              return (
                <tr key={automobile.id}>
                  <td>{automobile.vin}</td>
                  <td>{automobile.color}</td>
                  <td>{automobile.year}</td>
                  <td>{automobile.model.name}</td>
                  <td>{automobile.model.manufacturer.name}</td>
                  <td>{is_sold(automobile.sold)}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default AutomobilesList;
