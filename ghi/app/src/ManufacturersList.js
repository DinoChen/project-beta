import React from "react";

function ManufacturersList(props) {
  return (
    <>
      <div className="mt-5">
        <h1>Manufacturers</h1>
        <table className="table table-hover  table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {props.list.map((Manufactuerer) => {
              return (
                <tr key={Manufactuerer.id}>
                  <td>{Manufactuerer.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ManufacturersList;
