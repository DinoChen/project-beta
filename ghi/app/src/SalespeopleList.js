import React from "react";

export default function SalespeopleList(props) {
  return (
    <>
      <div className="mt-5">
        <h1>Salespoeple List</h1>
        <table className="table table-hover  table-striped">
          <thead>
            <tr>
              <th>Employee ID</th>
              <th>Fist Name</th>
              <th>Last Name</th>
            </tr>
          </thead>
          <tbody>
            {props.list.map((salesperson) => {
              return (
                <tr key={salesperson.id}>
                  <td>{salesperson.employee_id}</td>
                  <td>{salesperson.first_name}</td>
                  <td>{salesperson.last_name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
