import React from "react";

function ModelsList(props) {
  return (
    <>
      <div className="mt-5">
        <h1>Models</h1>
        <table className="table table-hover  table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {props.list.map((model) => {
              return (
                <tr key={model.id}>
                  <td>{model.name}</td>
                  <td>{model.manufacturer.name}</td>
                  <td>
                    <img
                      src={model.picture_url}
                      style={{ maxWidth: "300px", maxHeight: "300px" }}
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ModelsList;
