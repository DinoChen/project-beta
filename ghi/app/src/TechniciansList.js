import React from "react";

export default function TechnicansList(props) {
  return (
    <>
      <div className="mt-5">
        <h1>Technicans List</h1>
        <table className="table table-hover  table-striped">
          <thead>
            <tr>
              <th>Employee ID</th>
              <th>Fist Name</th>
              <th>Last Name</th>
            </tr>
          </thead>
          <tbody>
            {props.list.map((technician) => {
              return (
                <tr key={technician.id}>
                  <td>{technician.id}</td>
                  <td>{technician.first_name}</td>
                  <td>{technician.last_name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
