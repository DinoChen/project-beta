import React from "react";
import { useState, useEffect } from "react";
// import { formatDate, vipVin, formatTime } from "./AppointmentsList";

function formatDate(dateString) {
  // Create a Date object from the input string
  const date = new Date(dateString);

  // Extract year, month, and day components
  const year = date.getFullYear();
  const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Add 1 to month since months are 0-indexed
  const day = date.getDate().toString().padStart(2, "0");

  // Combine the components into the desired format
  return `${month}-${day}-${year}`;
}

// const inputt = "2023-04-25T13:18:00+00:00";
// const formattedDate = formatDate(inputt);

function formatTime(dateString) {
  // Create a Date object from the input string
  const date = new Date(dateString);

  // Extract hour, minute, and second components
  let hours = date.getUTCHours();
  console.log(hours);
  const minutes = date.getMinutes().toString().padStart(2, "0");
  const seconds = date.getSeconds().toString().padStart(2, "0");
  console.log("minutes", minutes);
  console.log("seconds", seconds);
  // Determine AM or PM
  const amOrPm = hours >= 12 ? "PM" : "AM";

  // Convert to 12-hour format
  hours = hours % 12;
  hours = hours ? hours : 12; // Replace 0 with 12 for display
  const hoursString = hours.toString().padStart(2, "0");

  // Combine the components into the desired format
  return `${hoursString}:${minutes}:${seconds} ${amOrPm}`;
}

function vipVin(is_vip) {
  const response = fetch(`http://localhost:8100/api/automobiles/`);
  const vips = [];
  if (response.ok) {
    const content = response.json();

    for (let vip of content.autos.vin) {
      vips = vips.push(vip);
    }
  }
  if (is_vip in vips) {
    return "Yes";
  } else {
    return "No";
  }
}
function ServiceHistory(props) {
  const [searchInput, setSearchInput] = useState("");
  const [vips, setVips] = useState([]);

  useEffect(() => {
    fetchVIPs();
  }, []);
  const fetchConfig = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };
  async function fetchVIPs() {
    try {
      const response = await fetch(
        `http://localhost:8100/api/automobiles/`,
        fetchConfig
      );

      if (response.ok) {
        const content = await response.json();
        const vips = content.autos.map((auto) => auto.vin);
        setVips(vips);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error("Error fetching VIPs:", error);
    }
  }

  function vipVins(vip) {
    return vips.includes(vip) ? "Yes" : "No";
  }

  function handleSearchInput(e) {
    setSearchInput(e.target.value);
  }

  const filteredList = props.list.filter((appointment) =>
    appointment.vin.toLowerCase().includes(searchInput.toLowerCase())
  );

  return (
    <>
      <div className="mt-5">
        <h1>Service History</h1>
        <input
          type="text"
          className="form-control mt-3 mb-3"
          placeholder="Search by VIN"
          value={searchInput}
          onChange={handleSearchInput}
        />
        <table className="table table-hover table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Is VIP</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technican</th>
              <th>Reason</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {filteredList.map((appointment) => {
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{vipVins(appointment.vin)}</td>
                  <td>{appointment.customer}</td>
                  <td>{formatDate(appointment.date_time)}</td>
                  <td>{formatTime(appointment.date_time)}</td>
                  <td>
                    {appointment.technician.first_name}{" "}
                    {appointment.technician.last_name}{" "}
                  </td>
                  <td>{appointment.reason}</td>
                  <td>{appointment.status}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ServiceHistory;
