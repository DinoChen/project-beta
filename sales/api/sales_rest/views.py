# from django.shortcuts import render
# from .models import AutomobileVO, Salesperson, Customer, Sale
# from django.views.decorators.http import require_http_methods
# from common.json import ModelEncoder
# from django.http import JsonResponse
# import json
# # Create your views here.
# class AutomobileVOListEncoder(ModelEncoder):
#     model = AutomobileVO
#     properties = [
#         'color',
#         'year',
#         'vin',
#         'cold',

#     ]


# class CustomerListEncoder(ModelEncoder):
#     model = Customer
#     properties = [
#         'id',
#         'first_name',
#         "last_name",
#         'address',
#         'phone_number'
#     ]
# class CustomerDetailEncoder(ModelEncoder):
#     model = Customer
#     properties = [
#         'first_name',
#         "last_name",
#         'address',
#         'phone_number'
#     ]

# class SalespersonListEncoder(ModelEncoder):
#     model = Salesperson
#     properties = [
#         'id',
#         'first_name',
#         'last_name',
#         'employee_id'
#     ]

# class SaleListEncoder(ModelEncoder):
#     model = Sale
#     properties = [
#         "automobile",
#         'salesperson',
#         'customer',
#         "price"
#     ]
#     encoders = {
#         'automobile':AutomobileVOListEncoder(),
#         'salesperson': Salesperson(),
#         'customer': CustomerListEncoder()
#     }


# @require_http_methods(["GET", "POST"])
# def api_salespeoples(request):
#     if request.method == 'GET':
#         salespople = Salesperson.objects.all()
#         return JsonResponse(
#             {"salespeople": salespople},
#             encoder=SalespersonListEncoder,
#         )
#     else:
#         try:
#             content = json.loads(request.body)
#             saleperson = Salesperson.objects.create(**content)
#             return JsonResponse(
#                 saleperson,
#                 encoder=SalespersonListEncoder,
#                 safe=False
#             )
#         except:
#             response = JsonResponse({ "message": "Could not create the salespeople"})
#             response.status_code = 400
#             return response

# @require_http_methods(["DELETE"])
# def api_salespeople(request, id):
#     if request.method == "DELETE":
#         try:
#             count, _ = Salesperson.objects.get(employee_id=id).delete()
#             return JsonResponse(
#                 {"deleted": count > 0}
#             )
#         except Salesperson.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid ID"},
#                 status = 400
#             )

# @require_http_methods(["GET", "POST"])
# def api_customers(request):
#     if request.method == 'GET':
#         customers = Customer.objects.all()
#         return JsonResponse(
#             {"customers": customers},
#             encoder=CustomerListEncoder,
#         )
#     else:
#         try:
#             content = json.loads(request.body)
#             customer = Customer.objects.create(**content)
#             return JsonResponse(
#                 customer,
#                 encoder=CustomerDetailEncoder,
#                 safe=False
#             )
#         except:
#             response = JsonResponse({"message": "Could not create the customer"})
#             response.status_code = 400
#             return response

# @require_http_methods(["DELETE"])
# def api_customer(request, id):
#     if request.method == "DELETE":
#         try:
#             count, _ = Customer.objects.get(id=id).delete()
#             return JsonResponse(
#                 {"deleted": count > 0}
#             )
#         except Customer.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid ID"},
#             )


# @require_http_methods(["GET", "POST"])
# def api_sales(request):
#     if request.method == 'GET':
#         sales = Sale.objects.all()
#         return JsonResponse(
#             {"sales": sales},
#             encoder=SaleListEncoder,
#         )
#     else:
#         try:
#             content = json.loads(request.body)
#             salesperson_id = content['salesperson']
#             customer_id = content['customer']
#             vin = content['automobile']

#             salesperson= Sale.objects.get(id=salesperson_id)
#             content['salesperson'] = salesperson


#             automobile = AutomobileVO.objects.get(vin=vin)
#             content['automobile'] = automobile

#             customer = Customer.objects.get(id=customer_id)
#             content['customer'] = customer
#             sale = Sale.objects.create(**content)
#             return JsonResponse(
#                 sale,
#                 encoder=SaleListEncoder,
#                 safe=False
#             )
#         except:
#             response = JsonResponse(
#                 {"message": "Could not create the sale"}
#             )
#             response.status_code = 400
#             return response

# @require_http_methods(["DELETE"])
# def api_sale(request, id):
#     if request.method == "DELETE":
#         try:
#             count, _ = Sale.objects.get(id=id).delete()
#             return JsonResponse(
#                 {"deleted": count > 0}
#             )
#         except Sale.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid ID"},
#                 status = 400
#             )











from django.shortcuts import get_object_or_404
from .models import Salesperson, Sale, AutomobileVO, Customer
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_or_create_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "PUT"])
def api_delete_or_update_salesperson(request, id):
    if request.method == "DELETE":
        salesperson = get_object_or_404(Salesperson, employee_id=id)
        count,_ = salesperson.delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(employee_id=id).update(**content)
        salesperson = Salesperson.objects.get(employee_id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )



@require_http_methods(["GET", "POST"])
def api_list_or_create_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "PUT"])
def api_delete_or_update_customer(request, id):
    if request.method == "DELETE":
        customer = get_object_or_404(Customer, id=id)
        count,_ = customer.delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )



@require_http_methods(["GET", "POST"])
def api_list_or_create_sale(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson_id = content["salesperson"]
            customer_id = content["customer"]
            automobile_vin = content["automobile"]

            vin = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = vin
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            content["salesperson"] = salesperson
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer


            sale = Sale.objects.create(**content)

            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except (AutomobileVO.DoesNotExist, Salesperson.DoesNotExist, Customer.DoesNotExist):
            response = JsonResponse({"message": "One of your objects does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
    if request.method == "DELETE":
        sale = get_object_or_404(Sale, id=id)
        count,_ = sale.delete()
        return JsonResponse({"deleted": count > 0})
