from django.contrib import admin
from .models import Sale, Salesperson, Customer, AutomobileVO
# Register your models here.

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    pass

@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass
